FROM openjdk:8

LABEL Description="Dockerfile for fhirobs2phmr service" Vendor="Alexandra Institute A/S" Version="1.0"

WORKDIR /opt/s4/service

ADD scripts/health.sh .
ADD target/service-jar-with-dependencies.jar .

CMD [ "java", "-Xdebug", "-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n", "-Dcom.sun.management.jmxremote.rmi.port=9010", "-Dcom.sun.management.jmxremote=true", "-Dcom.sun.management.jmxremote.port=9010", "-Dcom.sun.management.jmxremote.ssl=false", "-Dcom.sun.management.jmxremote.authenticate=false", "-Dcom.sun.management.jmxremote.local.only=false", "-Djava.rmi.server.hostname=localhost", "-jar", "service-jar-with-dependencies.jar"]
