package dk.s4.microservices.fhirobs2phmr.test;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import dk.s4.microservices.fhirobs2phmr.service.FHIRObs2PHMR;
import dk.s4.microservices.fhirobs2phmr.utils.FHIRRestUtils;
import org.apache.commons.io.FileUtils;
import org.hl7.fhir.r4.model.*;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.microservices.fhirobs2phmr.utils.PHMRFHIRUtils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FhirObs2PHMRTest {

    private final static Logger logger = LoggerFactory
            .getLogger(FhirObs2PHMRTest.class);

    private static final String ENVIRONMENT_FILE = "service.env";
    private static final String pulseObservationFileName = "FHIR_observation_pulse_manual.json";
    private static final String weightObservationFileName = "FHIR_observation_weight.json";
    private static final String ctgObservationFileName = "FHIR_observation_ctg.json";
    private static String pulseObsAsString;
    private static String weightObsAsString;
    private static String ctgObsAsString;
    private static FHIRRestUtils restUtils;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testFhirObs2PHMRForPulse() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        final Appender mockAppender = mock(Appender.class);
        when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

        PHMRFHIRUtils.convertfhirObs2PHMR(pulseObsAsString);

        verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
            @Override
            public boolean matches(final Object argument) {
                return ((LoggingEvent) argument).getFormattedMessage().contains("Produced complete PHMR document");

            }
        }));
    }

    @Test
    public void testFhirObs2PHMRForWeight() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        final Appender mockAppender = mock(Appender.class);
        when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

        PHMRFHIRUtils.convertfhirObs2PHMR(weightObsAsString);

        verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
            @Override
            public boolean matches(final Object argument) {
                return ((LoggingEvent) argument).getFormattedMessage().contains("Produced complete PHMR document");

            }
        }));
    }

    @Test
    public void testFhirObs2PHMRForCTG() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        final Appender mockAppender = mock(Appender.class);
        when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

        PHMRFHIRUtils.convertfhirObs2PHMR(ctgObsAsString);

        verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
            @Override
            public boolean matches(final Object argument) {
                return ((LoggingEvent) argument).getFormattedMessage().contains("Produced complete PHMR document");

            }
        }));
    }
    @BeforeClass
    public static void beforeClass() throws Exception {

        ClassLoader classLoader = FhirObs2PHMRTest.class.getClassLoader();

        String path = E2ETest.class.getClassLoader().getResource(".keep_fhirobs2phmr_service").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();

        logger.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        try {
            Properties properties = new Properties();
            FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
            properties.load(environmentFile);

            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
        }
        catch (IOException | IllegalArgumentException e) {
            logger.error("Failed loading file from: {}. Error message: {}",  path + "/" + ENVIRONMENT_FILE,
                    e.getMessage());
        }

        Boolean integrationTest = false;
        String integrationTestString = System.getenv("INTEGRATION_TEST");
        if (integrationTestString != null
                && !integrationTestString.isEmpty()
                && integrationTestString.equalsIgnoreCase("true"))
            integrationTest = true;

        if (!integrationTest) {
            Patient mockPatient = new Patient();
            mockPatient.addName().addGiven("Nancy Ann Test").setFamily("Berggren");
            mockPatient.addIdentifier().setSystem("urn:oid:1.2.208.176.1.2").setValue("2512489996");
            mockPatient.setId("http://example.com/patient_service/1");
            Observation observation = new Observation();
            observation
                    .setSubject(new Reference("patient"))
                    .setCode(new CodeableConcept());


            restUtils = mock(FHIRRestUtils.class);
            when(restUtils.getOrganization(any())).thenReturn(new Organization());
            when(restUtils.getPatient(any())).thenReturn(mockPatient);
            when(restUtils.getPractitioner(any())).thenReturn(new Practitioner());
            when(restUtils.getDeviceComponent(any())).thenReturn(new DeviceComponent());
            when(restUtils.getObservation(any())).thenReturn(new Observation());
            FHIRObs2PHMR.setRestUtils(restUtils);
        }

        File pulseFile = new File(classLoader.getResource(pulseObservationFileName)
                .getFile());
        pulseObsAsString = FileUtils.readFileToString(pulseFile,
                StandardCharsets.UTF_8);

        File weightFile = new File(classLoader.getResource(weightObservationFileName)
                .getFile());
        weightObsAsString = FileUtils.readFileToString(weightFile,
                StandardCharsets.UTF_8);

        File ctgFile = new File(classLoader.getResource(ctgObservationFileName)
                .getFile());
        ctgObsAsString = FileUtils.readFileToString(ctgFile,
                StandardCharsets.UTF_8);
    }
}
