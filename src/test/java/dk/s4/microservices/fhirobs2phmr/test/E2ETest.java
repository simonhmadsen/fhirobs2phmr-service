package dk.s4.microservices.fhirobs2phmr.test;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;
import com.google.common.io.Resources;
import dk.s4.microservices.fhirobs2phmr.service.FHIRObs2PHMR;
import dk.s4.microservices.fhirobs2phmr.utils.FHIRRestUtils;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.MessagingUtils;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.hl7.fhir.r4.model.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

/**
 * Tests contained in this file will start the FHIRObs2PHMR service, initialize
 * a Kafka producer and send and a FHIR Observation resource on topic
 * "observation". The tests succeed if the send succeeds and the FHIRObs2PHMR
 * service outputs
 * "Record sent to Kafka : <?xml version="1.0" encoding="UTF-8" standalone="
 * no"?>" (the latter part we do by testing on output from the root logBack
 * logger)
 *
 * Notice that the tests assume you have kafka running and setup (see
 * producer.props for assumed configuration.
 */

public class E2ETest {

    private final static Logger logger = LoggerFactory
            .getLogger(E2ETest.class);

    // TODO: Find real kafka topic
    private static final String INPUTTOPIC = "DataCreated_FHIR_Observation_MDC188736";
    private static final String OUTPUTTOPIC = "DataConverted_CDA_PHMR_MDC188736";

    private static KafkaProducer<String, String> mKafkaProducer;
    private static final String observationFileName = "FHIR_observation_weight.json";

    private static String fhirObsAsString;

    private static final String ENVIRONMENT_FILE = "service.env";
    private static final String PROPERTIES = "producer.props";

    private static FHIRRestUtils restUtils;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testConvertViaKafka() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory
                .getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

        final Appender mockAppender = mock(Appender.class);
        when(mockAppender.getName()).thenReturn("MOCK");
        root.addAppender(mockAppender);

        //Read resource from file
        Message kafkaMessage = new Message()
                .setBody(fhirObsAsString)
                .setSender("FHIROBS2PHMRServiceTest")
                .setCorrelationId(MessagingUtils.verifyOrCreateId(null))
                .setTransactionId(UUID.randomUUID().toString())
                .setBodyType("FHIR")
                .setContentVersion("R4");

        try {
            //Send resource string via KafkaProducer, and wait 10000 ms for Future to return
            logger.debug("Sending message to KafkaProducer: " + kafkaMessage.toString());
            ProducerRecord<String, String> record =
                    new ProducerRecord<>(INPUTTOPIC, kafkaMessage.toString());
            RecordMetadata metadata = mKafkaProducer.send(record).get(10000, TimeUnit.MILLISECONDS);
            logger.debug("message sent to KafkaProducer: offset = " + metadata.offset());
            assertNotNull(metadata);

            // Thread.sleep is a hack to have Kafka in other threads come
            // back with results.
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
                @Override
                public boolean matches(final Object argument) {
                    return ((LoggingEvent) argument).getFormattedMessage().contains("Produced complete PHMR document");
                }
            }));
            verify(mockAppender).doAppend(argThat(new ArgumentMatcher() {
                @Override
                public boolean matches(final Object argument) {
                    return ((LoggingEvent) argument).getFormattedMessage().contains("sendMessage: Message sent to topic = "+OUTPUTTOPIC);
                }
            }));
        }
        catch ( CancellationException | ExecutionException | InterruptedException | TimeoutException e) {
            logger.error("Error in testCreateViaKafka: ", e);
            assertNotNull(null);
        }
    }

    @BeforeClass
    public static void beforeClass() throws Exception {
        /*
         * This runs under maven, and I'm not sure how else to figure out the target directory from code..
         */
        String path = E2ETest.class.getClassLoader().getResource(".keep_fhirobs2phmr_service").getPath();
        path = new File(path).getParent();
        path = new File(path).getParent();
        path = new File(path).getParent();
        logger.info("Project base path is: {}", path);

        //Read and set environment variables from .env file:
        try {
            Properties properties = new Properties();
            FileInputStream environmentFile = new FileInputStream(path + "/" + ENVIRONMENT_FILE);
            logger.info("Path: " + path);
            properties.load(environmentFile);
            properties.forEach((k, v)->environmentVariables.set((String)k, (String)v));
        }
        catch (IOException | IllegalArgumentException e) {
            logger.error("Failed loading file from: {}. Error message: {}", path + "/" + ENVIRONMENT_FILE,
                    e.getMessage());
        }

        //Read KafkaProducer properties and init producer from these
        try (InputStream props = Resources.getResource(PROPERTIES).openStream()) {
            Properties properties = new Properties();
            properties.load(props);
            mKafkaProducer = new KafkaProducer<>(properties);
        } catch (IOException | IllegalArgumentException e) {
            logger.error("Failed loading file from: {}. Error message: {}",  PROPERTIES,
                    e.getMessage());
        }

        // Check whether tests should be integration or unit tests
        Boolean integrationTest = false;
        String integrationTestString = System.getenv("INTEGRATION_TEST");
        if (integrationTestString != null
                && !integrationTestString.isEmpty()
                && integrationTestString.equalsIgnoreCase("true"))
            integrationTest = true;

        if (!integrationTest) {
            logger.debug("Using mocks");
            Patient mockPatient = new Patient();
            mockPatient.addName().addGiven("Nancy Ann Test").setFamily("Berggren");
            mockPatient.addIdentifier().setSystem("urn:oid:1.2.208.176.1.2").setValue("2512489996");
            mockPatient.setId("http://example.com/patient_service/1");
            Observation observation = new Observation();
            observation
                    .setSubject(new Reference("patient"))
                    .setCode(new CodeableConcept());


            restUtils = mock(FHIRRestUtils.class);
            when(restUtils.getOrganization(any())).thenReturn(new Organization());
            when(restUtils.getPatient(any())).thenReturn(mockPatient);
            when(restUtils.getPractitioner(any())).thenReturn(new Practitioner());
            when(restUtils.getDeviceComponent(any())).thenReturn(new DeviceComponent());
            when(restUtils.getObservation(any())).thenReturn(new Observation());

            FHIRObs2PHMR.setRestUtils(restUtils);
        } else {
            logger.debug("Not using mocks");
        }

        ClassLoader classLoader = E2ETest.class.getClassLoader();
        File file = new File(classLoader.getResource(observationFileName).getFile());
        fhirObsAsString = FileUtils.readFileToString(file, StandardCharsets.UTF_8);

        logger.info("Starting FHIRObs2PHMR.main");
        FHIRObs2PHMR.main(new String[] {});
    }

    @AfterClass
    public static void afterClass() throws Exception {
        mKafkaProducer.close();
    }
}
