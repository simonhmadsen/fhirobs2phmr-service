package dk.s4.microservices.fhirobs2phmr.kafka;

import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.fhirobs2phmr.service.FHIRObs2PHMR;
import dk.s4.microservices.fhirobs2phmr.utils.FhirContextUtil;
import dk.s4.microservices.fhirobs2phmr.utils.PHMRFHIRUtils;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyEventProcessor implements EventProcessor {
    private final Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);

    @Override
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage) {
        logger.debug("Processing message from topic {}", consumedTopic.toString());

        outgoingMessage.setBodyType("CDA")
                .setContentVersion("DK-PHMR-1.3")
                .setCorrelationId(receivedMessage.getCorrelationId())
                .setTransactionId(receivedMessage.getTransactionId());

        // convert FHIR to PHMR
        try {
            String phmr = PHMRFHIRUtils.convertfhirObs2PHMR(receivedMessage.getBody());
            if (phmr == null) {
                Exception e = new Exception("Something went wrong while processing observation");
                onError(receivedMessage, outgoingMessage, e.getMessage());
                setKafkaTopic(messageProcessedTopic, consumedTopic, false);
                return false;
            }
            outgoingMessage.setBody(phmr);
        } catch(Exception e) {
            logger.error("Processing failed: {}", e.getMessage(), e);
            onError(receivedMessage, outgoingMessage, e.getMessage());
            setKafkaTopic(messageProcessedTopic, consumedTopic, false);
            return false;
        }

        setKafkaTopic(messageProcessedTopic, consumedTopic, true);

        return true;
    }

    private void setKafkaTopic(Topic messageProcessedTopic, Topic consumedTopic,  boolean success) {
        messageProcessedTopic
                .setEvent("DataConverted")
                .setDataCatagory("CDA")
                .setDataType("PHMR")
                .setDataCode(consumedTopic.getDataCode());


        if (!success) messageProcessedTopic.setEvent("ProcessingFailed");
    }

    private void onError(Message receivedMessage, Message outgoingMessage, String errorMsg) {

        IParser parser = FhirContextUtil.getFhirContext().newJsonParser();

        OperationOutcome outcome = new OperationOutcome();
        outcome.addIssue()
                .setSeverity(OperationOutcome.IssueSeverity.ERROR)
                .setDetails(new CodeableConcept()
                        .setText(errorMsg));
        outgoingMessage
                .setBody(parser.encodeResourceToString(outcome));
    }
}