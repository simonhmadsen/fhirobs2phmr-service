package dk.s4.microservices.fhirobs2phmr.service;

import dk.s4.microservices.fhirobs2phmr.kafka.MyEventProcessor;
import dk.s4.microservices.fhirobs2phmr.utils.FHIRRestUtils;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;

import static net.logstash.logback.argument.StructuredArguments.kv;

/**
 * The FHIRObs2PHMR Service subscribes on messages from Kafka on the topic named
 * "InputReceived_FHIR_Observation_MDC29112". The service converts the input data
 * (FHIR observation format) into HL7 PHMR format, and writes the converted data on
 * Kafka on a new topic named "DataConverted_CDA_PHMR_MDC29112" (as an XML string).
 * 
 * TODO Topic names may change
 * 
 * NOTE! This service could be made simpler and smarter by using the
 * KafkaStreams Java library which has been released in Kafka 0.10.x.x Instead
 * of implementing separate Kafka consumer and producer, the KafkaStreams
 * library is about stream processing: i.e. consuming data from kafka,
 * performing operations on data and sending the data to kafka again - in only
 * few lines of code!
 * 
 */
public class FHIRObs2PHMR {
	private final static Logger logger = LoggerFactory
			.getLogger(FHIRObs2PHMR.class);

	//The following to be able to inject a mock PatientResolver from test code
	public static FHIRRestUtils restUtils = null;

	public static void setRestUtils(FHIRRestUtils newRestUtils) {
		restUtils = newRestUtils;
	}

	public static FHIRRestUtils getRestUtils() {
		if (restUtils == null)
			restUtils = new FHIRRestUtils();
		return restUtils;
	}

	public static void main(String[] args) throws IOException {
		LinkedList<String> topics = new LinkedList<>();
		Topic topic1 = new Topic()
				.setEvent("DataCreated")
				.setDataCatagory("FHIR")
				.setDataType("Observation")
				.setDataCode("MDC8450059");
		Topic topic2 = new Topic()
				.setEvent("DataCreated")
				.setDataCatagory("FHIR")
				.setDataType("Observation")
				.setDataCode("MDC188736");
		topics.add(topic1.toString());
		topics.add(topic2.toString());

		logger.debug("Starting FHIR2PHMR service {}",
				kv("topic", topics.isEmpty() ? "" : topics.get(0) + ", " + topics.get(1)));

		try {
			KafkaEventProducer kafkaEventProducer = new KafkaEventProducer("FHIRObs2PHMR-service");
			EventProcessor eventProcessor = new MyEventProcessor();
			new Thread((new KafkaConsumeAndProcess(topics, kafkaEventProducer, eventProcessor))).start();
		} catch (KafkaInitializationException | MessagingInitializationException e) {
			logger.error("Error during Kafka initialization: ", e);
			System.exit(1);
		}
	}
}
