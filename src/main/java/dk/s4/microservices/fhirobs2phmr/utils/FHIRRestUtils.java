package dk.s4.microservices.fhirobs2phmr.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class FHIRRestUtils {
    private final static Logger logger = LoggerFactory
            .getLogger(FHIRRestUtils.class);

    private static IGenericClient observationService = null;

    /**
     * Retrieves the FHIR patient resource specified by id
     *
     * @param id
     *            FHIR patient resource id
     * @return FHIR patient resource
     */
    public org.hl7.fhir.r4.model.Patient getPatient(IdType id) {
        logger.debug("Requesting Patient with id = " + id);
        org.hl7.fhir.r4.model.Patient patient = null;

        try {
            // Perform a search
            FhirContext ctx = FhirContextUtil.getFhirContext();

            IGenericClient client;
            if (id.getBaseUrl() != null) {
                client = ctx
                        .newRestfulGenericClient(id.getBaseUrl());
            } else {
                throw new IllegalArgumentException("The resource " + id + "does not contain a baseurl.");
            }
            patient = client.read().resource(org.hl7.fhir.r4.model.Patient.class)
                    .withId(id.getIdPart()).execute();
            logger.debug("Retrieved Patient with id = "
                    + patient.getId());

        } catch (Exception e) {
            logger.error("Failed getting patient resource: ", e);
        }

        return patient;
    }

    /**
     * Retrieves the FHIR observation resource specified by id
     *
     * @param id
     *            FHIR observation resource id
     * @return FHIR observation resource
     */
    public org.hl7.fhir.r4.model.Observation getObservation(IdType id) {
        logger.debug("Requesting Observation with id = " + id);
        org.hl7.fhir.r4.model.Observation observation = null;

        try {
            // Perform a search
            FhirContext ctx = FhirContextUtil.getFhirContext();;
            if(observationService == null) {
                observationService = ctx.newRestfulGenericClient(System.getenv().get("OBSERVATION_SERVICE_URL"));
            }
            observation = observationService.read().resource(org.hl7.fhir.r4.model.Observation.class)
                    .withId(id.getIdPart()).execute();
            logger.debug("Retrieved Observation with id = "
                    + observation.getId());

        } catch (Exception e) {
            logger.error("Failed getting observation resource: ", e);
        }

        return observation;
    }

    /**
     * Retrieves the FHIR device component resource specified by id
     *
     * @param id
     *            FHIR device component resource id
     * @return FHIR device component resource
     */
    public org.hl7.fhir.r4.model.DeviceComponent getDeviceComponent(IdType id) {
        logger.debug("Requesting DeviceComponent with id = " + id);
        org.hl7.fhir.r4.model.DeviceComponent deviceComponent = null;

        try {
            // Perform a search
            FhirContext ctx = FhirContextUtil.getFhirContext();;
            if(observationService == null) {
                observationService = ctx.newRestfulGenericClient(System.getenv().get("OBSERVATION_SERVICE_URL"));
            }
            deviceComponent = observationService.read().resource(org.hl7.fhir.r4.model.DeviceComponent.class)
                    .withId(id.getIdPart()).execute();
            logger.debug("Retrieved DeviceComponent with id = "
                    + deviceComponent.getId());

        } catch (Exception e) {
            logger.error("Failed getting device component resource: ", e);
        }

        return deviceComponent;
    }

    /**
     * Retrieves the FHIR organization resource specified by id
     *
     * @param id
     *            FHIR organization resource id
     * @return FHIR organization resource
     */
    public org.hl7.fhir.r4.model.Organization getOrganization(IdType id) {
        logger.debug("Requesting Organization with id = " + id);
        org.hl7.fhir.r4.model.Organization organization = null;

        try {
            // Perform a search
            FhirContext ctx = FhirContextUtil.getFhirContext();;
            IGenericClient client;
            if (id.getBaseUrl() != null) {
                client = ctx
                        .newRestfulGenericClient(id.getBaseUrl());
            } else {
                throw new IllegalArgumentException("The resource " + id + "does not contain a baseurl.");
            }
            organization = client.read().resource(org.hl7.fhir.r4.model.Organization.class)
                    .withId(id.getIdPart()).execute();
            logger.debug("Retrieved organization with id = "
                    + organization.getId());

        } catch (Exception e) {
            logger.error("Failed getting organization resource: ", e);
        }

        return organization;
    }

    /**
     * Retrieves the FHIR practitioner resource specified by id
     *
     * @param id
     *            FHIR practitioner resource id
     * @return FHIR practitioner resource
     */
    public org.hl7.fhir.r4.model.Practitioner getPractitioner(IdType id) {
        logger.debug("Requesting Practitioner with id = " + id);
        org.hl7.fhir.r4.model.Practitioner practitioner = null;

        try {
            // Perform a search
            FhirContext ctx = FhirContextUtil.getFhirContext();
            IGenericClient client;
            if (id.getBaseUrl() != null) {
                client = ctx
                        .newRestfulGenericClient(id.getBaseUrl());
            } else {
                throw new IllegalArgumentException("The resource " + id + "does not contain a baseurl.");
            }
            practitioner = client.read().resource(org.hl7.fhir.r4.model.Practitioner.class)
                    .withId(id.getIdPart()).execute();
            logger.debug("Retrieved practitioner with id = "
                    + practitioner.getId());

        } catch (Exception e) {
            logger.error("Failed getting practitioner resource: ", e);
        }

        return practitioner;
    }
}
