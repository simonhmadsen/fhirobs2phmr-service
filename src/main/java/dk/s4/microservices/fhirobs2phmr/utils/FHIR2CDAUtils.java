package dk.s4.microservices.fhirobs2phmr.utils;

import ca.uhn.fhir.context.FhirContext;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class FHIR2CDAUtils {
    private final static Logger logger = LoggerFactory
            .getLogger(FHIR2CDAUtils.class);

    private static FhirContext fhirContext = null;

    /**
     * Based on FHIR practitioner resource builds PersonIdentity information for PHMR
     *
     * @param practitioner
     *            FHIR Practitioner resource
     * @return PersonIdentity
     */
    protected static PersonIdentity buildPersonIdentity(org.hl7.fhir.r4.model.Practitioner practitioner) {

        fhirContext = FhirContextUtil.getFhirContext();

        logger.debug("Building CDA PersonIdentity from Practitioner: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(practitioner));
        PersonIdentity personIdentity;

        PersonIdentity.PersonBuilder personBuilder = new PersonIdentity.PersonBuilder(
            practitioner.getNameFirstRep().getFamily());
        for(StringType givenName : practitioner.getNameFirstRep().getGiven()) {
            personBuilder.addGivenName(givenName.getValue());
        }
        personIdentity = personBuilder.setPrefix(practitioner.getNameFirstRep().getPrefixAsSingleString())
            .build();

        return personIdentity;
    }


    /**
     * Based on FHIR patient resource builds Patient information for PHMR
     *
     * @param patient
     *            FHIR Patient resource
     * @return Patient
     */
    protected static dk.s4.hl7.cda.model.Patient buildPatient(org.hl7.fhir.r4.model.Patient patient) {

        fhirContext = FhirContextUtil.getFhirContext();

        logger.debug("Building CDA Patient from FHIR Patient: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(patient));
        dk.s4.hl7.cda.model.Patient pt;

        String patientCpr = "";

        List<String> givenNames = new ArrayList<>();
        String familyName = "";
        List<Telecom> telecomList = null;
        AddressData addressData = null;
        Date birthDate = null;

        List<Identifier> identifierList = patient.getIdentifier();
        for (Identifier identifier : identifierList) {
            // Look for DK CPR identifier
            if (identifier.getSystem().endsWith("1.2.208.176.1.2")) {
                patientCpr = patient.getIdentifier().get(0).getValue();
            }
        }

        if (!patientCpr.isEmpty()) {
            for(StringType given: patient.getName().get(0).getGiven()) {
                givenNames.add(given.toString());
            }

            familyName = patient.getName().get(0).getFamily();
            addressData = buildAddressData(patient.getAddressFirstRep());

            telecomList = buildTelecomList(patient.getTelecom());

            birthDate = patient.getBirthDate();
        }


        Patient.PatientBuilder patientBuilder = new dk.s4.hl7.cda.model.Patient.PatientBuilder(familyName);
                for(String given: givenNames) {
                    patientBuilder.addGivenName(given);
                }
                patientBuilder.setBirthTime(birthDate)
                .setSSN(patientCpr)
                .setAddress(addressData);

        if (telecomList != null && !telecomList.isEmpty()) {
            for(Telecom telecom: telecomList) {
                patientBuilder.addTelecom(telecom.getAddressUse(), telecom.getProtocol(), telecom.getValue());
            }
        }

        pt = patientBuilder.build();

        return pt;
    }

    /**
     * Based on FHIR organization resource builds OrganizationIdentity information for PHMR
     *
     * Using mock data until organisation services are in place
     *
     * @return OrganizationIdentity
     */
    protected static OrganizationIdentity buildOrganizationIdentity(Organization organization) {

        fhirContext = FhirContextUtil.getFhirContext();

        logger.debug("Building CDA Organization from FHIR Organization: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(organization));

        OrganizationIdentity organizationIdentity;

        String SOR = null; // TODO: Is SOR mandatory?
        for (Identifier identifier : organization.getIdentifier()) {
            if (identifier.getSystem().endsWith("1.2.208.176.1.1")) {
                SOR = identifier.getValue();
            }
        }

        AddressData organisationAddress = null;
        if (!organization.getAddress().isEmpty())
            organisationAddress = buildAddressData(organization.getAddressFirstRep());

        List<Telecom> telecomList = buildTelecomList(organization.getTelecom());

        OrganizationIdentity.OrganizationBuilder organizationBuilder = new OrganizationIdentity.OrganizationBuilder()
                .setSOR(SOR)
                .setName(organization.getName())
                .setAddress(organisationAddress);

        if (!telecomList.isEmpty()) {
            for(Telecom telecom: telecomList) {
                organizationBuilder.addTelecom(telecom.getAddressUse(), telecom.getProtocol(), telecom.getValue());
            }
        }

        organizationIdentity = organizationBuilder.build();

        return organizationIdentity;
    }

    /**
     * Based on FHIR observation resource builds Measurement information for PHMR
     *
     * @return Measurement
     */
    protected static Measurement buildMeasurement(Observation observation) {
        fhirContext = FhirContextUtil.getFhirContext();

        logger.debug("Building CDA Measurement from FHIR Observation: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(observation));

        Measurement measurement = null;

        try {
            // statusCode
            Measurement.Status measurementStatus = Measurement.Status.COMPLETED;
            if (observation.getStatus().equals("cancelled")
                    || observation.getStatus().equals("entered-in-error"))
                measurementStatus = Measurement.Status.NULLIFIED;

            // id
            ID measurementId = new ID.IDBuilder()
                    .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
                    .setExtension(UUID.randomUUID().toString())
                    .setRoot(MedCom.ROOT_OID).build();

            // value
            Quantity quantityValue = null;
            SampledData sampledData = null;
            String value = null;


            if (observation.hasValueQuantity()) {
                quantityValue = observation.getValueQuantity();
                value = quantityValue.getValue().toString();
            } else if (observation.hasValueSampledData()){
                sampledData = observation.getValueSampledData();
                value = sampledData.getData();
            } else {
                quantityValue = new Quantity();
                quantityValue.setCode("0");
                quantityValue.setSystem("0");
                value = "0";
            }
            // code
            String MDCSystem = "2.16.840.1.113883.6.24";
            String alternateMDCSystem = "urn:iso:std:iso:11073:10101";
            String MDCCode = null;
            String MDCDisplayName = null;
            for(Coding code: observation.getCode().getCoding()) {
                if (code.getSystem().contains(MDCSystem) || code.getSystem().contains(alternateMDCSystem)) {
                    MDCCode = code.getCode();
                    MDCDisplayName = code.getDisplay();
                }
            }

            // methodCode
            // TODO: Get from observation
            DataInputContext context = new DataInputContext(
                    DataInputContext.ProvisionMethod.TypedByCitizen,
                    DataInputContext.PerformerType.Citizen);


            Measurement.MeasurementBuilder measurementBuilder = new Measurement.MeasurementBuilder(
                    observation.getEffectiveDateTimeType().getValue(), measurementStatus)
                    .setContext(context);
            if (quantityValue != null) {
                measurementBuilder.setPhysicalQuantity(
                        value,
                        UCUMUtil.getUnit(
                                quantityValue.getSystem().substring(quantityValue.getSystem().lastIndexOf(":") + 1, quantityValue.getSystem().length()),
                                quantityValue.getCode()),
                        MDCCode,
                        MDCDisplayName,
                        MDCSystem,
                        MDCDisplayName
                );
            } else {
                measurementBuilder
                        .setPhysicalQuantity(
                                value,
                                UCUMUtil.getUnit(
                                        sampledData.getOrigin().getSystem().substring(sampledData.getOrigin().getSystem().lastIndexOf(":") + 1, sampledData.getOrigin().getSystem().length()),
                                        sampledData.getOrigin().getCode()),
                                MDCCode,
                                MDCDisplayName,
                                MDCSystem,
                                MDCDisplayName
                        );
            }
            measurementBuilder.setId(measurementId);
            measurement = measurementBuilder.build();
        } catch (FHIRException e) {
            e.printStackTrace();
        }

        return measurement;
    }

    /**
     * Based on FHIR deviceComponent resource builds MedicalEquipment information for PHMR
     *
     * @return MedicalEquipment
     */
    protected static MedicalEquipment buildMedicalEquipment(DeviceComponent deviceComponent) {
        fhirContext = FhirContextUtil.getFhirContext();

        logger.debug("Building CDA MedicalEquipment from FHIR DeviceComponent: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(deviceComponent));

        MedicalEquipment medicalEquipment;

        // code
        String MDCSystem = "2.16.840.1.113883.6.24";
        String alternateMDCSystem = "urn:iso:std:iso:11073:10101";
        String MDCCode = null;

        for(Coding code: deviceComponent.getType().getCoding()) {
            if (code.getSystem().equals(MDCSystem) || code.getSystem().equals(alternateMDCSystem)) {
                MDCCode = code.getCode();
            }
        }

        String manufacturer = null;
        String modelName = null;
        String softwareRevision = null;
        String serialNumber = null;

        for(DeviceComponent.DeviceComponentProductionSpecificationComponent spec: deviceComponent.getProductionSpecification()) {
            for(Coding coding: spec.getSpecType().getCoding()) {
                if (coding.getSystem().equals("urn:iso:std:iso:11073:10101")) {
                    switch (coding.getCode()) {
                        case "531970":
                            manufacturer = spec.getProductionSpec();
                            break;
                        case "531969":
                            modelName = spec.getProductionSpec();
                            break;
                        case "531975":
                            softwareRevision = spec.getProductionSpec();
                            break;
                        case "531972":
                            serialNumber = spec.getProductionSpec();
                            break;
                        default:
                            break;

                    }
                }
            }
        }

        if (softwareRevision == null) {
            softwareRevision = "No software specified";
        }
        medicalEquipment = new MedicalEquipment.MedicalEquipmentBuilder()
                .setMedicalDeviceCode(MDCCode)
                .setMedicalDeviceDisplayName(manufacturer + " - " + modelName)
                .setManufacturerModelName("Manufacturer: " + manufacturer + " / Model: " + modelName)
                .setSerialNumber(serialNumber)
                .setSoftwareName(softwareRevision)
                .build();

        return medicalEquipment;
    }

    /**
     * Based on FHIR Address resource builds AddressData information for QRD
     *
     * @param address
     *            FHIR Address resource
     * @return AddressData
     */
    protected static AddressData buildAddressData(Address address) {

        fhirContext = FhirContextUtil.getFhirContext();
        AddressData addressData;

        AddressData.AddressBuilder addressBuilder = new AddressData.AddressBuilder(
                address.getPostalCode(), address.getCity());
        for (StringType addressLine : address.getLine()) {
            addressBuilder.addAddressLine(addressLine.getValue());
        }
        addressData = addressBuilder.setCountry(address.getCountry())
                .setUse(AddressData.Use.WorkPlace)
                .build();

        return addressData;
    }

    /**
     * Based on list of FHIR ContactPoint resources builds List of Telecom information for QRD
     t
     * @param contactPoints
     *            List of FHIR ContactPoint resources
     * @return List<Telecom>
     */
    protected static List<Telecom> buildTelecomList(List<ContactPoint> contactPoints) {

        fhirContext = FhirContextUtil.getFhirContext();

        ArrayList<Telecom> telecomList = new ArrayList<>();

        for (ContactPoint contactPoint: contactPoints) {
            String protocol = null;
            AddressData.Use use;
            String value;

            switch (contactPoint.getSystem()) {
                case PHONE:
                    protocol = "tel";
                    break;
                case EMAIL:
                    protocol = "mailto";
                    break;
                case FAX: case PAGER: case URL: case SMS: case OTHER:
                    protocol = null;
                    break;
            }

            switch (contactPoint.getUse()) {
                case HOME:
                    use = AddressData.Use.HomeAddress;
                    break;
                case WORK:
                    use = AddressData.Use.WorkPlace;
                    break;
                default:
                    use = AddressData.Use.HomeAddress;
                    break;
            }

            value = contactPoint.getValue();

            telecomList.add(
                    new Telecom(use, protocol, value)
            );
        }

        return telecomList;
    }
}
