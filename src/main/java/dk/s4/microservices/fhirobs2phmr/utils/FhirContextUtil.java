package dk.s4.microservices.fhirobs2phmr.utils;

import ca.uhn.fhir.context.FhirContext;

public class FhirContextUtil {

    private static FhirContext instance = null;

    protected FhirContextUtil() { }

    public static FhirContext getFhirContext() {
        if(instance == null) {
            instance = FhirContext.forR4();
        }
        return instance;
    }
}