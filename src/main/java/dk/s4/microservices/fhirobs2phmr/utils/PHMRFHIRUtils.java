package dk.s4.microservices.fhirobs2phmr.utils;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.google.common.io.Resources;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.microservices.fhirobs2phmr.service.FHIRObs2PHMR;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Reference;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Utility class for converting FHIR observation resources to HL7 PHMR format
 * resource.
 */
public class PHMRFHIRUtils {
	private final static Logger logger = LoggerFactory
			.getLogger(PHMRFHIRUtils.class);

	private static FhirContext fhirContext = null;
	private static final String CUSTODIAN_ENV = "custodian_config.env";
	private static final String AUTHOR_ENV = "author_config.env";
	private static final String LEGAL_AUTHENTICATOR_ENV = "legal_authenticator_config.env";

	/**
	 * Converts a FHIR observation resource to HL7 PHMR
	 *
	 * @param observationString
	 *            The FHIR observation resource as a string to be converted into
	 *            PHMR.
	 * @return the PHMR XML document as a string
	 */
	public static String convertfhirObs2PHMR(String observationString) {

		FHIRRestUtils rest = FHIRObs2PHMR.getRestUtils();
		fhirContext = FhirContextUtil.getFhirContext();

		String phmrString = "";
		PHMRDocument phmr = null;

		try {
			// Notice: we assume JSON, not XML for FHIR resources
			IParser parser = fhirContext.newJsonParser();

			logger.debug("Parsing FHIR Observation to Object");

			Observation observation = parser.parseResource(Observation.class,
					observationString);

			logger.debug("FHIR Observation parsed to object");

			DateTimeType effectiveTime = observation.getEffectiveDateTimeType();

			// PHMR Header

			Reference subject = observation.getSubject();
			if (subject != null && !subject.isEmpty() && !subject.getReference().isEmpty()) {
				org.hl7.fhir.r4.model.Patient patient = rest.getPatient(new IdType(subject.getReference()));
				if (patient != null && !patient.isEmpty()) {
					Patient cdaPatient = FHIR2CDAUtils.buildPatient(patient);
					if (cdaPatient != null) {

						// id
						String documentId = "";
						for (Identifier identifier : patient.getIdentifier()) {
							if (identifier.getSystem().equals("urn:oid:1.2.208.184")) {
								documentId = identifier.getValue();
								break;
							}
						}
						if (documentId != null && documentId.isEmpty()) documentId = UUID.randomUUID().toString();

						phmr = new PHMRDocument(new ID.IDBuilder()
								.setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
								.setExtension(documentId)
								.setRoot(MedCom.ROOT_OID).build()
						);

						// title
						phmr.setTitle("Hjemmemonitorering for " + cdaPatient.getIdValue());

						// effectiveTime TODO: Shouldn't effectiveTime be the time this document was created?
						phmr.setEffectiveTime(effectiveTime.getValue());

						// languageCode TODO: Can we get around hardcoding languageCode?
						phmr.setLanguageCode("da-DK");

						// recordTarget
						phmr.setPatient(cdaPatient);

						// author - uses mockdata TODO: Find out from where we get author

						Properties authorConfig = null;
						try (InputStream authorEnv = Resources.getResource(AUTHOR_ENV).openStream()) {
							authorConfig = new Properties();

							authorConfig.load(authorEnv);
						}
						catch (IOException | IllegalArgumentException e) {
							logger.error("Failed loading file from: {}. Error message: {}",  AUTHOR_ENV,
									e.getMessage());
						}

						OrganizationIdentity authorOrganisationIdentity = null;
						PersonIdentity authorPersonIdentity = null;
						// If an author organization is present in the fhir observation
						if (authorConfig != null) {
							// Check if author organization should be parsed from config-file
							if(Boolean.parseBoolean(authorConfig.getProperty("AUTHOR_ORGANIZATION"))) {
								Organization custodianOrganization = ENV2FHIRUtils.organizationFromProperties(authorConfig);
								authorOrganisationIdentity = FHIR2CDAUtils.buildOrganizationIdentity(custodianOrganization);
							}
							// Check if author person should be parsed from config-file
							if (Boolean.parseBoolean(authorConfig.getProperty("AUTHOR_PERSON"))) {
								Practitioner authorPractitioner = ENV2FHIRUtils.practitionerFromProperties(authorConfig);
								authorPersonIdentity = FHIR2CDAUtils.buildPersonIdentity(authorPractitioner);
							}
						}
						// TODO: Add call to getOrganization and getPractitioner i FHIRRestUtils when it is clear where to get author from.

						Participant.ParticipantBuilder participantBuilder = new Participant.ParticipantBuilder();
						if (authorOrganisationIdentity != null || authorPersonIdentity != null) {
							Date documentCreationTime = new DateTime().toDate();

							ID id = (authorOrganisationIdentity != null) ? authorOrganisationIdentity.getId() : null;


							participantBuilder
									.setId(id)
									.setOrganizationIdentity(authorOrganisationIdentity)
									.setPersonIdentity(authorPersonIdentity)
									.setTime(documentCreationTime)
									.setId(authorOrganisationIdentity != null ? authorOrganisationIdentity.getId() : null)
									.setAddress(authorOrganisationIdentity != null ? authorOrganisationIdentity.getAddress() : null);
						}

						phmr.setAuthor(participantBuilder.build());

						// custodian

						Properties custodianConfig = null;
						// Get custodian information from environment-file
						try (InputStream custodianEnv = Resources.getResource(CUSTODIAN_ENV).openStream()) {
							custodianConfig = new Properties();

							custodianConfig.load(custodianEnv);
						}
						catch (IOException | IllegalArgumentException e) {
							logger.error("Failed loading file from: {}. Error message: {}",  CUSTODIAN_ENV,
									e.getMessage());
						}

						OrganizationIdentity custodianOrganizationIdentity = null;
						if (custodianConfig != null) {
							// Check if custodian should be parsed from config-file
							if(Boolean.parseBoolean(custodianConfig.getProperty("CUSTODIAN_ORGANIZATION"))) {
								Organization custodianOrganization = ENV2FHIRUtils.organizationFromProperties(custodianConfig);
								custodianOrganizationIdentity = FHIR2CDAUtils.buildOrganizationIdentity(custodianOrganization);
							}
						}
						// TODO: Add call to getOrganization and getPractitioner i FHIRRestUtils when it is clear where to get custodian from.

						phmr.setCustodian(custodianOrganizationIdentity);

						// legalAuthenticator

						Properties legalAuthenticatorConfig = null;
						try (InputStream authorEnv = Resources.getResource(LEGAL_AUTHENTICATOR_ENV).openStream()) {
							legalAuthenticatorConfig = new Properties();

							legalAuthenticatorConfig.load(authorEnv);
						}
						catch (IOException | IllegalArgumentException e) {
							logger.error("Failed loading file from: {}. Error message: {}",  LEGAL_AUTHENTICATOR_ENV,
									e.getMessage());
						}

						OrganizationIdentity legalAuthenticatorOrganizationIdentity = null;
						PersonIdentity legalAuthenticatorPersonIdentity = null;
						// If an author organization is present in the fhir observation
						if (legalAuthenticatorConfig != null) {
							// Check if author organization should be parsed from config-file
							if(Boolean.parseBoolean(legalAuthenticatorConfig.getProperty("LEGAL_AUTHENTICATOR_ORGANIZATION"))) {
								Organization legalAuthenticatorOrganization = ENV2FHIRUtils.organizationFromProperties(legalAuthenticatorConfig);
								legalAuthenticatorOrganizationIdentity = FHIR2CDAUtils.buildOrganizationIdentity(legalAuthenticatorOrganization);
							}
							// Check if author person should be parsed from config-file
							if (Boolean.parseBoolean(legalAuthenticatorConfig.getProperty("LEGAL_AUTHENTICATOR_PERSON"))) {
								Practitioner legalAuthenticatorPractitioner = ENV2FHIRUtils.practitionerFromProperties(legalAuthenticatorConfig);
								legalAuthenticatorPersonIdentity = FHIR2CDAUtils.buildPersonIdentity(legalAuthenticatorPractitioner);
							}
						}
						// TODO: Add call to getOrganization and getPractitioner i FHIRRestUtils when it is clear where to get legal authenticator from.

						if (legalAuthenticatorOrganizationIdentity != null ||  legalAuthenticatorPersonIdentity != null) {
							Date legalAuthenticationTime = new DateTime().toDate();

							participantBuilder = new Participant.ParticipantBuilder()
									.setOrganizationIdentity(legalAuthenticatorOrganizationIdentity)
									.setPersonIdentity(legalAuthenticatorPersonIdentity)
									.setTime(legalAuthenticationTime)
									.setId(legalAuthenticatorOrganizationIdentity != null ? legalAuthenticatorOrganizationIdentity.getId() : null)
									.setAddress(legalAuthenticatorOrganizationIdentity != null ? legalAuthenticatorOrganizationIdentity.getAddress() : null);
						}

						phmr.setLegalAuthenticator(participantBuilder.build());

						// documentationOf
						phmr.setDocumentationTimeInterval(
								effectiveTime.getValue(),
								effectiveTime.getValue());
					}
				}
			}

			// PHMR Body
			if (phmr != null) {

				List<Observation> observations = new ArrayList<>();
				if (observation != null && !observation.isEmpty()) {
					observations.add(observation);
				}

				if (observation.hasHasMember()) {
					for (Reference reference: observation.getHasMember()) {
						if (reference != null && !reference.isEmpty() && !reference.getReference().isEmpty()) {
							Observation member = rest.getObservation(new IdType(reference.getReference()));
							if (member != null && !member.isEmpty()) {
								observations.add(member);
							}
						}
					}
				}
				for (Observation obs: observations) {
					Measurement measurement = FHIR2CDAUtils.buildMeasurement(obs);

					switch (measurement.getCode()) {
						case "150016": case "150017": case "150018": case "150019": case "150020": case "150021": case "150022": case "150023":
							phmr.addVitalSign(measurement); // Blood pressure
							break;
						case "150364": case "150392": case "188420": case "188424": case "188428": case "188432": case "188448":
							phmr.addVitalSign(measurement); // Core Body Temperature
							break;
						case "150320": case "150456":
							phmr.addVitalSign(measurement); // O2 saturation
							break;
						case "151562":
							phmr.addVitalSign(measurement); // Respiration Rate
							break;
						case "149514": case "149546": case "149530":
							phmr.addVitalSign(measurement); // Pulse
							break;
						default:
							phmr.addResult(measurement);
							break;
					}
				}

				// Medical Equipment
				Reference deviceReference = observation.getDevice();
				if (deviceReference != null && !deviceReference.isEmpty() && !deviceReference.getReference().isEmpty()) {
					DeviceComponent deviceComponent = rest.getDeviceComponent(new IdType(deviceReference.getReference()));
					if (deviceComponent != null) {
						// Check if devicecomponent has a parentreference and add it if it does
						if (deviceComponent.getParent() != null && !deviceComponent.getParent().isEmpty() && !deviceComponent.getParent().getReference().isEmpty()) {
							DeviceComponent parentDeviceComponent = rest.getDeviceComponent(new IdType(deviceReference.getReference()));
							if (parentDeviceComponent != null) {
								MedicalEquipment parentMedicalEquipment = FHIR2CDAUtils.buildMedicalEquipment(parentDeviceComponent);
								phmr.addMedicalEquipment(parentMedicalEquipment);
							}
						}
						MedicalEquipment medicalEquipment = FHIR2CDAUtils.buildMedicalEquipment(deviceComponent);
						phmr.addMedicalEquipment(medicalEquipment);
					}
				}
			}

			// PHMR to String
			PHMRXmlCodec phmrCodec = new PHMRXmlCodec();
            System.out.println("phmr: "+ phmr);
            if (phmr != null) {
				phmrString = phmrCodec.encode(phmr);
			}

			logger.trace("PHMR document: " + phmrString);

			boolean completePHMR = phmrString.contains("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>")
					&& phmrString.contains("</ClinicalDocument>");

			if (completePHMR) {
				logger.debug("Produced complete PHMR document");
				logger.debug("PHMR document: {}...", phmrString.substring(0, 2000));
			} else
				logger.error("Incomplete PHMR document");

		} catch (FHIRException e) {
			e.printStackTrace();
		}

		return phmrString;
	}
}
