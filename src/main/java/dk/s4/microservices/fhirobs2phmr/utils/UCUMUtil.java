package dk.s4.microservices.fhirobs2phmr.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class UCUMUtil {

    private final static Logger logger = LoggerFactory
            .getLogger(UCUMUtil.class);

    private static final Map<String, String> MDC2UCUM_MAP = Collections.unmodifiableMap(createMDCMap());
    private static final Map<String, Map<String, String>> SYSTEM_MAP = Collections.unmodifiableMap(createSystemMap());

    private static Map<String, Map<String, String>> createSystemMap() {
        Map<String, Map<String, String>> map = new HashMap<>();

        map.put("2.16.840.1.113883.6.24", MDC2UCUM_MAP);
        map.put("10101", MDC2UCUM_MAP);
        map.put("0", MDC2UCUM_MAP);

        return map;
    }

    private static Map<String, String> createMDCMap() {
        Map<String, String> map = new HashMap<>();

        map.put("266016", "mm[Hg]"); // Millimeters of mercury
        map.put("268192", "Cel"); // Degrees Celcius
        map.put("262688", "%"); // Percent
        map.put("264928", "{breaths}/min"); // Respiratory Rate
        map.put("264864", "{beats}/min"); // Pulse
        map.put("265987", "kPa"); // KiloPascal
        map.put("263441", "cm"); // Centimeters
        map.put("263520", "[in_i]"); // Inches
        map.put("263904", "[lb_av]"); // Pounds
        map.put("263875", "kg"); // Kilograms
        map.put("264320", "s"); // Seconds
        map.put("266419", "uV"); // Seconds
        map.put("0", "No unit"); // Kilograms

        return map;
    }

    /**
     * Returns a UCUM unit string given a system and a code
     * @param system the system
     * @param code the code
     * @return String
     */
    public static String getUnit(String system, String code) throws IllegalArgumentException {
        Map<String, String> systemMap = SYSTEM_MAP.get(system);
        if (systemMap == null) {
            throw new IllegalArgumentException("The provided system is not known");
        }
        String unit = systemMap.get(code);
        if (unit == null) {
            throw new IllegalArgumentException("The provided code is not known");
        }
        return unit;
    }
}
