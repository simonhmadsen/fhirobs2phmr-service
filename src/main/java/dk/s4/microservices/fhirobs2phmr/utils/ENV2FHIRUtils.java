package dk.s4.microservices.fhirobs2phmr.utils;

import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ENV2FHIRUtils {
    private final static Logger logger = LoggerFactory
            .getLogger(ENV2FHIRUtils.class);

    /**
     * Based on env-file in "resources" dir builds FHIR Organization resource
     *
     * @param props
     *            env-file containing data regarding organization as Properties object
     * @return Organization
     */
    public static Organization organizationFromProperties(Properties props) {
        logger.debug("Creating Organization from Properties");

        Organization organization = new Organization();

        Address address = new Address()
                .setCountry(props.getProperty("CUSTODIAN_ORGANIZATION_COUNTRY"))
                .setCity(props.getProperty("CITY"))
                .setPostalCode(props.getProperty("POSTAL_CODE"))
                .addLine(props.getProperty("LOCATION_NAME"))
                .addLine(props.getProperty("STREET_NAME"));

        organization.addAddress(address)
                .setName(props.getProperty("ORGANIZATION_NAME"))
                .addIdentifier()
                .setUse(Identifier.IdentifierUse.OFFICIAL)
                .setSystem("urn:oid:1.2.208.176.1.1")
                .setValue(props.getProperty("SOR"));

        return organization;
    }

    /**
     * Based on env-file in "resources" dir builds FHIR Practitioner resource
     *
     * @param props
     *            env-file containing data regarding practitioner as Properties object
     * @return Practitioner
     */
    public static Practitioner practitionerFromProperties(Properties props) {
        logger.debug("Creating Practitioner from Properties");

        Practitioner practitioner;

        practitioner = new org.hl7.fhir.r4.model.Practitioner()
                .addName(new HumanName()
                        .addGiven(props.getProperty("GIVEN_NAME"))
                        .addPrefix(props.getProperty("PREFIX"))
                        .setFamily(props.getProperty("FAMILY_NAME")));

        return practitioner;
    }
}
