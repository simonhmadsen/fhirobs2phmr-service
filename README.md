# FHIR-Observation To PHMR Service:

| Summary                          |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | fhirobs2phmr-service                                                                                                        |
| Short description                | Converts FHIR Observation resources into HL7 PHMR format |
| Lastest version                  | 2.4.12                                                                                                               |
| Maturity level                   | Prototype                                                                                                           |
| Documentation                    | https://bitbucket.org/4s/fhirobs2phmr-service/wiki/Home                                                       |
| Used externally                  | No                                                                   |
| Database                         | No                                                                                                                  |
| RESTful dependencies             | n/a   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | User context service (user context info from session ID)                                                            |
| License                          | Apache 2.0                                                                                                          |

The FHIRObs2PHMR-service subscribes on messages from Kafka on the topics named "InputReceived_FHIR_Observation_MDC8450059" and "InputReceived_FHIR_Observation_MDC188736". 

The service converts the input data (FHIR observation format) into HL7 PHMR format, and writes 
the converted data on Kafka on a new topic named "DataConverted_CDA_PHMR_<MDCCODE>" (as an XML string).

## Further documentation

Design and external API documentation: See https://bitbucket.org/4s/fhirobs2phmr-service/wiki/Home.

Javadoc can be generated using the command:
```
javadoc -sourcepath src/main/java -subpackages dk.s4.microservices.fhirobs2phmr -d <destination-folder>
```

## Prerequisites
You need to have the following software installed:

  * Java 1.8.0+
  * Maven 3.1.1+
  * Docker 1.13+
  
The project depends on git submodules (ms-scripts). To initialize these you need to execute:

```bash
git submodule update --init
git submodule foreach git pull origin master
```
  
In the service.env file you need to set ```KAFKA_BOOTSTRAP_SERVER``` to point to a running Kafka broker. By default it is set
to point to ```kafka:9092``` which is the address Kafka will run at if you run it locally via this project: http://4s-online.dk/wiki/doku.php?id=4smicroservices:kafka.
  
## Run
```
mvn clean package  -DskipTests
java -jar target/service-jar-with-dependencies.jar
```
## Test
```
mvn test
```

#### Environment variables
To run the tests, environment variables for the kafka-service have to be set. 
These variables can be set in the service.env and in the src > test > resources directory in the producer.props files respectively.

In the service.env file, the most important settings are the 
```KAFKA_BOOTSTRAP_SERVER``` and the ```KAFKA_GROUP_ID``` variables.

The ```KAFKA_BOOTSTRAP_SERVER``` variable needs to point to a running kafka broker.
If you are running kafka locally via this project: http://4s-online.dk/wiki/doku.php?id=4smicroservices:kafka
this variable can be set to ```kafka:9092```

The ```KAFKA_GROUP_ID``` variable needs to be set to the name of the consumer group, the Kafka consumer should belong to.

## Docker
Building and running the service using docker is described in the following.

#### To build docker images locally
The simplest way to build the service is to use the buildall.sh script:

````bash
/scripts/buildall.sh
````

This will build the service as a WAR file and add this to a Jetty based docker image. Now, to run the service and the accompanying database execute:

````bash
docker-compose up
````

#### Logging with fluentd
If you want use the fluentd logging driver (if you for instance have setup fluentd, elasticsearch and Kibana) instead of the standard json-file one start the service like this:

```bash
docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up
```

#### To stop the docker service
```bash
docker-compose -f docker-compose.yml down
```

#### Environment variables
To run this service with docker, som environment variables have to be set.

In the service.env file you need to set ```KAFKA_BOOTSTRAP_SERVER``` to point to a running Kafka broker. By default it is set
to point to ```kafka:9092``` which is the address Kafka will run at if you run it locally via this project: http://4s-online.dk/wiki/doku.php?id=4smicroservices:kafka. 

